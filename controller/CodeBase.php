<?php

namespace xhyadminframework\controller;

use think\facade\Db;
use think\facade\View;
use xhyadminframework\base\XhyController;
use xhyadminframework\model\Menu as MenuModel;
use xhyadminframework\Utils;
use xhyadminframework\utils\FileUtil;

class CodeBase extends XhyController
{
    public function generator(): string
    {
        $mysqlVersion = Db::query('select version() as version');
        return "保存成功";
    }

    //控制器生成
    public function creatAllFiles()
    {
        $tableName =input('tableName', 'test');
        $prefix =input('prefix','');
        $table = $prefix.$tableName;
        $isTable = Db::query("SHOW TABLES LIKE '{$table}'");
        if(!$isTable){
            return "未找到相关表";
        }

        $moduleName= input('moduleName', 'app');
        $isLogin = input('isLogin', 0, 'intval');
        $rootpath = input('root_path',"/home/www/phpProject/cloud_coaching/backend/web");
        $catalogue1 = input('views_child_catalogue',"teach");
        $catalogue2 = input('views_grandson_catalogue','test');
        $menuName = input('menuName', '测试');
        if(empty($rootpath))return "请填写重要参数path";
        $path = $rootpath.DIRECTORY_SEPARATOR."src".DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR.$catalogue1.DIRECTORY_SEPARATOR.$this->columNameToVarName($catalogue2).DIRECTORY_SEPARATOR;
        $columNameToVarNameTable = $this->columNameToVarName($tableName);
        $res = $this->generateControllerFile($tableName,$moduleName,$isLogin,$prefix);
        $vueseR = $this->createVueFile($path,$catalogue1,$catalogue2,$rootpath,$table,$columNameToVarNameTable);
        $vueCreateRs = $this->createVueCreateFile($path,$catalogue1,$catalogue2,$table);
        $jsCreateRs = $this->createJSFile($rootpath.DIRECTORY_SEPARATOR."src".DIRECTORY_SEPARATOR."api".DIRECTORY_SEPARATOR.$catalogue1.DIRECTORY_SEPARATOR,$catalogue2,$tableName);
        //插入菜单数据
        $columNameToVarNameCatalogue2 =$this->columNameToVarName($catalogue2);

        $menuData =[
            'component_name'=>$columNameToVarNameCatalogue2,
            'is_enabled'=> 1,
            'is_expand'=> 1,
            'is_protected'=> 1,
            'link_page'=> "/{$catalogue1}/{$columNameToVarNameCatalogue2}/index",
            'menu_name'=> $menuName,
            'menu_parent_id'=> 0,
            'module'=> $catalogue1,
            'permission_mark'=> $columNameToVarNameTable."@index",
            'small_icon'=> "ordered-list"
        ];
        $funError="";
        $lastId = $this->addMenu($menuData);
        if(strpos($lastId,'请手动添加') !== false){
            return $res."\r\n".$vueseR."\r\n".$vueCreateRs."\r\n".$jsCreateRs."\r\n".$lastId;
        }else{
            $functions=[
                ["name"=>"新增","identity"=>"{$columNameToVarNameTable}@save"],
                ["name"=>"修改","identity"=>"{$columNameToVarNameTable}@update"],
                ["name"=>"删除","identity"=>"{$columNameToVarNameTable}@delete"],
                ["name"=>"批量删除","identity"=>"{$columNameToVarNameTable}@delList"]
            ];
            foreach ($functions as $item){
                $flag = $this->onFunctionSaveData([
                    'component_name' => "",
                    'function_identity'=> $item['identity'],
                    'function_name'=> $item['name'],
                    'menu_id'=> $lastId,
                    'permission_id'=> $item['identity'],
                    'sort_number'=> 1
                ]);
                if(strpos($lastId,'请手动添加') !== false){
                    $funError .= ",".$flag;
                }
            }
        }
        return $res."\r\n".$vueseR."\r\n".$vueCreateRs."\r\n".$jsCreateRs."\r\n".$funError;
    }

    //生成控制器文件
    public function generateControllerFile($tableName, $moduleName, $loginFlag,$prefix)
    {
        //资源控制器
        $modelPath = '..' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR;
        if (!file_exists($modelPath)) {
            FileUtil::createDir($modelPath);
        }
        $filePath = $modelPath . $this->tableNameToModelName($tableName) . ".php";
        if (file_exists($filePath)) {
            return "有相同控制器，已为你跳过<br>";
        }else{
            $code = $this->generateControllerCode($tableName, $moduleName);
            file_put_contents($filePath, $code);

            //自动注册资源路由
            $controllername = $this->tableNameToModelName($tableName);
            $replace = "
    Route::get('{$this->columNameToVarName($tableName)}', '\\{$moduleName}\\controller\\{$controllername}@index');
    Route::get('{$this->columNameToVarName($tableName)}/<id>', '\\{$moduleName}\\controller\\{$controllername}@read');
    Route::post('{$this->columNameToVarName($tableName)}', '\\{$moduleName}\\controller\\{$controllername}@save');
    Route::put('{$this->columNameToVarName($tableName)}', '\\{$moduleName}\\controller\\{$controllername}@update');
    Route::delete('{$this->columNameToVarName($tableName)}/<id>', '\\{$moduleName}\\controller\\{$controllername}@delete');
    Route::post('{$this->columNameToVarName($tableName)}/delList', '\\{$moduleName}\\controller\\{$controllername}@delList');
        ";
            if ($loginFlag == 1) {
                $routefile = '..' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . 'route.php';
                $findStr = "})->middleware(config('xhy.route_middleware'));";

                $routeContent = file_get_contents($routefile);
                $newRouteContent = str_replace($findStr,$replace . "\r\n" . $findStr, $routeContent);
                file_put_contents($routefile, $newRouteContent);
            } else {
                $routefile = '..' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . 'notAuthRoute.php';
                file_put_contents($routefile, $replace, FILE_APPEND);
            }
        }

        //创建模型
        $modelres = $this->createModelFile($tableName, $moduleName,$prefix);

        return '生成成功，生成路径为：<br>资源控制器：' . $filePath . '<br>模型：' . $modelres . '<br>';
    }


    //把带下划线命名转换为驼峰命名（首字母大写）
    public function tableNameToModelName($tableName)
    {
        $tempArray = explode('_', $tableName);
        $result = "";
        for ($i = 0; $i < count($tempArray); $i++) {
            $result .= ucfirst($tempArray[$i]);
        }
        return $result;
    }

    //生成模型文件
    public function generateControllerCode($tableName, $moduleName)
    {
        $initTableName = $this->tableNameToModelName($tableName);
        View::assign('modalname', $initTableName);
        View::assign('tableName', $this->tableNameToModelName($tableName));
        View::assign('moduleName', $moduleName);
        //apiDoc
        View::assign('rest', $this->columNameToVarName($tableName));

        $codeBasePath = app()->getRootPath() . 'framework' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
        $template = file_get_contents($codeBasePath . 'Controller' . DIRECTORY_SEPARATOR . 'controller.html');//读取模板.
        View::config(['view_path' => $codeBasePath . 'Controller' . DIRECTORY_SEPARATOR]);
        return "<?php\r\n" . View::display($template);
    }


    //把带下划线命名转换为驼峰命名（首字母小写）
    function columNameToVarName($columName)
    {
        $tempArray = explode('_', $columName);
        $result = "";
        for ($i = 0; $i < count($tempArray); $i++) {
            $result .= ucfirst($tempArray[$i]);
        }
        return lcfirst($result);
    }

    //生成资源控制器代码
    public function createModelFile($tableName, $moduleName,$prefix)
    {
        $modelPath = '../' . $moduleName . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR;
        if (!file_exists($modelPath)) {
            FileUtil::createDir($modelPath);
        }
        $code = $this->generateModelCode($tableName, $moduleName,$prefix);
        $filePath = $modelPath . $this->tableNameToModelName($tableName) . ".php";
        if (file_exists($filePath)) {
            return "有相同的模型，已为你跳过<br>";
        }
        file_put_contents($filePath, $code);
        return $filePath;
    }

    //生成模型代码源码
    public function generateModelCode($tableName, $moduleName,$prefix)
    {
        View::assign('tableName', $this->tableNameToModelName($tableName));
        View::assign('moduleName', $moduleName);
        View::assign('trueTable', $prefix.$tableName);
        View::assign('pk', 'id');
        $codeBasePath = app()->getRootPath() . 'framework' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
        $template = file_get_contents($codeBasePath . 'Model' . DIRECTORY_SEPARATOR . 'model.html');//读取模板
        View::config(['view_path' => $codeBasePath . 'Model' . DIRECTORY_SEPARATOR]);
        return "<?php\r\n" . View::display($template);
    }

    //生成Vue index文件
    private function createVueFile($path,$catalogue1,$catalogue2,$rootpath,$tableName,$columNameToVarNameTable){
        if(!file_exists($path)){
            FileUtil::createDir($path);
        }
        $fileP = $path  . "index.vue";
        if (file_exists($fileP)) {
            return "有相同index文件，已为你跳过<br>";
        }
        $code = $this->generateVueCode($catalogue1,$catalogue2,$tableName,$columNameToVarNameTable);
        file_put_contents($fileP, $code);
        //对前端componentsMap.js页面进行编写
        $componentsMapJSPath = $rootpath.DIRECTORY_SEPARATOR."src".DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."componentsMap.js";
        $a = $this->columNameToVarName($catalogue2);
        $replace ="    {$a}: () => import('@/views/{$catalogue1}/{$a}/index'),\r\n";
        $routeContent = file_get_contents($componentsMapJSPath);
        $newRouteContent =  substr_replace($routeContent,$replace,strripos($routeContent,"}"),0);
        file_put_contents($componentsMapJSPath, $newRouteContent);
        //对前端components.js进行编写
        $componentsJSPath = $rootpath.DIRECTORY_SEPARATOR."src".DIRECTORY_SEPARATOR."enums".DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR."components.js";
        $a = $this->columNameToVarName($catalogue2);
        $routeContent = file_get_contents($componentsJSPath);
        $content = substr_replace($routeContent,"        '{$a}',\r\n",strripos($routeContent,"]"),0);
        file_put_contents($componentsJSPath, $content);
        return '生成index页面成功，生成路径为：<br>' . $fileP;
    }


    //生成Vue index代码
    public function generateVueCode($catalogue1,$catalogue2,$tableName,$columNameToVarNameTable){
        //注册模版参数
        View::assign('catalogue1', $catalogue1);
        View::assign('catalogue2', $this->tableNameToModelName($catalogue2));
        View::assign('catalogue3', $this->columNameToVarName($catalogue2));
        View::assign('catalogue4', $columNameToVarNameTable);
        $rs = Db::query("show COLUMNS FROM {$tableName}");
        $cloumnstr = "[";
        foreach ($rs as $key=>$item){
            $cloumnstr .="{
                    title: '{$item['Field']}',
                    dataIndex: '{$item['Field']}',
                    align: 'center'
                   },";
        }
        $cloumnstr .= "{
                    title: '操作',
                    dataIndex: 'action',
                    width: '200px',
                    scopedSlots: {
                        customRender: 'action'
                    },
                    align: 'center',
                }";

        View::assign('cloumns', $cloumnstr."]");
        $codeBasePath = app()->getRootPath() . 'framework' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
        $template = file_get_contents($codeBasePath . 'Vue' . DIRECTORY_SEPARATOR . 'index.html');//读取模板
        View::config(['view_path' => $codeBasePath . 'Vue' . DIRECTORY_SEPARATOR]);
        return View::display($template);
    }

    //生成Vue文件
    private function createVueCreateFile($path,$catalogue1,$catalogue2,$tableName){
        $fileP = $path  . "create.vue";
        if (file_exists($fileP)) {
            return "有相同create文件,已为你跳过<br>";
        }
        $code = $this->generateVueCreateCode($catalogue1,$catalogue2,$tableName);
        file_put_contents($fileP, $code);

        return '生成create页面成功，生成路径为：<br>' . $fileP;
    }
    //生成Vue代码
    public function generateVueCreateCode($catalogue1,$catalogue2,$tableName){
        //注册模版参数
        View::assign('catalogue1', $catalogue1);
        View::assign('lcCatalogue2', $this->columNameToVarName($catalogue2));
        View::assign('pk', 'id');
        $rs = Db::query("show COLUMNS FROM {$tableName}");
        View::assign('rows', $rs);
        $updateCloumns = "";$valiCloumns="";$rowList="";
        foreach ($rs as $key=>$item){
            if($item['Field']!='id'){
                $updateCloumns .="'{$item['Field']}',";
                if($item['Null']=='NO'){
                    $valiCloumns.="'{$item['Field']}',";
                }
            }
            $rowStr="''";
            if(!empty($item['Default'])){
                $rowStr = is_numeric($item['Default'])?$item['Default']:"'{$item['Default']}'";
            }
            $rowList .="{$item['Field']}:$rowStr,";
        }
        $updateCloumns =rtrim($updateCloumns,",");
        $valiCloumns =rtrim($valiCloumns,",");
        $rowCloumns =rtrim($rowList,",");
        View::assign('cloumsList', $updateCloumns);
        View::assign('valiCloumns', $valiCloumns);
        View::assign('rowCloumns', $rowCloumns);
        $codeBasePath = app()->getRootPath() . 'framework' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
        $template = file_get_contents($codeBasePath . 'Vue' . DIRECTORY_SEPARATOR . 'create.html');//读取模板
        View::config(['view_path' => $codeBasePath . 'Vue' . DIRECTORY_SEPARATOR]);
        return View::display($template);
    }

    //生成Vue index文件
    private function createJSFile($path,$catalogue2,$route){
        if(!file_exists($path)){
            FileUtil::createDir($path);
        }
        $fileP = $path  . $this->columNameToVarName($catalogue2).".js";
        if (file_exists($fileP)) {
            return "有相同JS文件,已为你跳过<br>";
        }
        $code = $this->generateJSCode($route);
        file_put_contents($fileP, $code);

        return '生成JS页面成功，生成路径为：<br>' . $fileP;
    }
    //生成Vue index代码
    public function generateJSCode($route){
        //注册模版参数
        View::assign('route', $this->columNameToVarName($route));
        $codeBasePath = app()->getRootPath() . 'framework' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
        $template = file_get_contents($codeBasePath . 'Vue' . DIRECTORY_SEPARATOR . 'js.html');//读取模板
        View::config(['view_path' => $codeBasePath . 'Vue' . DIRECTORY_SEPARATOR]);
        return View::display($template);
    }

    public function addMenu($param){
        try {
            $logTitle = "添加菜单" . $param['menu_name'] . '成功';
            // 如果是子分类 自动写入父类模块
            $menuModel = new MenuModel();
            $parentId = $param['menu_parent_id'] ?? 0;
            $param['menu_treeid'] = self::getTreeId($parentId);
            $param['created'] = Utils::now();
            $param['creater'] = $this->userName;
            $param['menu_id'] = Utils::guid();
            $param['sort_number'] = self::getSortNumb($parentId);
            $menuModel->insert($param);
            $isSuccess = true;
            $logDetail = "";
            $this->logInfo($logTitle, $logDetail);
            Db::commit();
            return $param['menu_id'];
        } catch (\Exception $exception) {
            //region 异常 (回滚事务/记录日志/返回错误)
            Db::rollback();
            $this->logError("添加/修改出错", $exception);
            return $exception->getMessage().",添加菜单出错,请手动添加<br>";
            //endregion
        } finally {
            //region 结束
            if ($isSuccess == false) {
                Db::rollback();
            }
        }
    }

    /**
     * 通过当前菜单的父菜单id获取树节点ID
     * @param $parent_menu_id
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function getTreeId($parent_menu_id)
    {
        $menuModel = new MenuModel();
        $tree = '00001';
        //  获取当前最大的树节点id
        // $maxMenuTreeId = $menuModel->where('menu_parent_id', $parent_menu_id)->order('menu_treeid', 'desc')->limit(1)->select();
        $query = "select max(menu_treeid) as max_treeid from s_menu where menu_parent_id='" . $parent_menu_id . "'";
        $max = Db::query($query);
        $maxMenuTreeId = $max[0]['max_treeid'];

        // 如果当前菜单的父菜单id为0  即当前菜单是顶级菜单 新增时用
        if ($parent_menu_id == "0") {
            // 顶级菜单 的最大值存在 则新的树id 为 最大值+1
            $tree = (str_pad($maxMenuTreeId + 1, strlen($maxMenuTreeId), "0", STR_PAD_LEFT));

        } else {
            // 如果不是顶级菜单
            // 如果有最大值  最大值+1
            if (strlen($maxMenuTreeId) > 0) {
                $tree = (str_pad($maxMenuTreeId + 1, strlen($maxMenuTreeId), "0", STR_PAD_LEFT));
            } else {
                // 如果没有 需要获取父节点的treeid
                $menuInfo = $menuModel->where('menu_id', $parent_menu_id)->find();

                $tree = $menuInfo->menu_treeid . $tree;
            }

        }
        return $tree;
    }
    /**
     * 获取父节点最大的sort_numb
     * @param $parentMenuId
     * @return int|mixed
     */
    private function getSortNumb($parentMenuId)
    {
        $menuModel = new MenuModel();
        $max = $menuModel->where('menu_parent_id', $parentMenuId)->max('sort_number');
        return ($max + 1);

    }
    private function onFunctionSaveData($param)
    {
        $isSuccess = false;

        $logModel = new MenuModel();
        //判断添加或修改

        Db::startTrans();
        try {
            $where = $param['menu_id'];
            //日志
            $logTitle = "添加" . $param['function_name'] . "成功";
            $param['menu_function_id'] = Utils::guid();
            $param['created'] = Utils::now();
            $param['creater'] = $this->userName;

            $menuAction = Db::table('s_menu_function')
                ->where('menu_id', $where)
                ->whereRaw("'function_name'=:function_name or 'function_identity'=:function_identity ", ['function_name' => $param['function_name'], 'function_identity' => $param['function_identity']])
                ->find();
            if ($menuAction) {
                return'功能标识/控制器名称已存在<br>';
            }
            $logModel->insertFunction($param);

            $logDetail = "";
            $this->logInfo($logTitle, $logDetail);
            $isSuccess = true;
            Db::commit();
            return "新增操作详情成功<br>";
        } catch (\Exception $exception) {
            //region 异常 (回滚事务/记录日志/返回错误)
            Db::rollback();
            $this->logError("添加/修改出错", $exception);
            return $exception->getMessage().",请手动添加{$param['function_name']}<br>";
            //endregion
        } finally {
            //region 结束
            if ($isSuccess == false) {
                Db::rollback();
            }
        }
    }
}
